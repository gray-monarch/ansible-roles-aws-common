---
# tasks file for gray-monarch.ec2-from-ami

# Yes this is ugly. What's happening here is that I'm running a loop with the
# gm_ec2_instance_settings dict defined in the higher play. The complex variables
# complete the logic of filtering values through a set of hierarchies.
# Values for this task can be defined in multiple places. The defaults in
# this role's defaults.yml have the lowest priority, then the dictionary's top-level
# then the item's own attributes.

- name: Launch pre-existing AWS AMI [ec2-from-ami]
  ec2:
    region:         "{{ item.value.region            | default(gm_ec2_instance_settings.region)        | default(gm_ec2_instance_defaults.region) }}"
    key_name:       "{{ item.value.key_name          | default(gm_ec2_instance_settings.key_name)      | default(gm_ec2_instance_defaults.key_name) }}"
    instance_type:  "{{ item.value.instance_type     | default(gm_ec2_instance_settings.instance_type) | default(gm_ec2_instance_defaults.instance_type) }}"
    image:          "{{ item.value.image             | default(gm_ec2_instance_settings.image)         | default(gm_ec2_instance_defaults.image) }}"
    exact_count:    "{{ item.value.exact_count       | default(gm_ec2_instance_settings.exact_count)   | default(gm_ec2_instance_defaults.exact_count) }}"
    group:          "{{ item.value.group|default([]) |   union(gm_ec2_instance_settings.group)         | default(gm_ec2_instance_defaults.group) }}"
    instance_tags:  "{{ gm_ec2_instance_defaults.instance_tags | combine(gm_ec2_instance_settings.instance_tags|default({})) | combine(item.value.instance_tags) }}"
    count_tag:
      Role: "{{ item.value.instance_tags.Role | default(gm_ec2_instance_settings.role) | mandatory }}"
    wait: true
  with_dict: "{{ gm_ec2_instance_settings.instances }}"
  register: gm_ec2_instance_facts
  loop_control:
    label:
      "{{
          ' Name: ' ~ (  (gm_ec2_instance_defaults.instance_tags | combine(item.value.instance_tags)).Name ) ~
          ' Image: ' ~ ( item.value.image | default(gm_ec2_instance_defaults.image) )
      }}"

# This should negate the need to manually populate ec2_tag names below, as of
# 2.2, see: https://github.com/ansible/ansible/pull/15890
- meta: refresh_inventory

- name: Join tagged and new AWS EC2 instances to inventory group [ec2-from-ami]
  add_host:
    name: "{{ item.public_ip }}"
    groups: "{{ gm_ec2_inventory_group_name }}"
    ec2_tag_User: "{{ item.tags.User }}"
    ec2_tag_OS: "{{ item.tags.OS }}"
    ec2_tag_Role: "{{ item.tags.Role }}"
    ec2_tag_Status: "{{ item.tags.Status }}"
    ec2_tag_Name: "{{ item.tags.Name }}"
    ec2_tag_FQDN: "{{ item.tags.FQDN | default(omit) }}"
    ec2_ip_address: "{{ item.public_ip }}"
    ec2_region: "{{ item.region }}"
    ec2_private_ip_address: "{{ item.private_ip }}"
    ec2_id: "{{ item.id }}"
    # This ties into "Attempt to use key manually" from ec2-key-maker
    # by setting the key file manually if the var is defined
    ansible_ssh_private_key_file: "{{ gm_ec2_ssh_private_key_file | default(omit) }}"
  with_items: "{{ gm_ec2_instance_facts_list }}"
  changed_when: false
  loop_control:
    label:
      "{{
          ' Name: ' ~ ( item.tags.Name | default('no-name') ) ~
          ' Group: ' ~ gm_ec2_inventory_group_name
      }}"

- name: Wait for AWS EC2 instances to have SSH [ec2-from-ami]
  wait_for:
    host: "{{ item }}"
    port: "{{ gm_ec2_wait_for_port }}"
    search_regex: OpenSSH
  with_items: "{{ groups[gm_ec2_inventory_group_name] }}"

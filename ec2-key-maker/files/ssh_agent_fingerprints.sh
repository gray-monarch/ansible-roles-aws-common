#!/bin/bash

# We will exit if ssh-add fails and use the bad exit status in ansible
# Exit status of 1 means no identities, 2 means no agent available
SSH_KEY_LIST="$(ssh-add -L)" || exit $?

# Right now we just want RSA keys because Amazon doesn't support ED25519
mapfile -t agent_key_array <<< $( echo "$SSH_KEY_LIST" | awk '$0 ~ "^ssh-rsa"' )

# Output the MD5 fingerprints of all SSH-Agent RSA keys
for ((i=0; i<${#agent_key_array[@]}; i++)); do
  ssh-keygen -m PKCS8 -e -f <( printf '%s' "${agent_key_array[i]}" ) \
    | openssl rsa -pubin -outform DER 2>/dev/null \
    | openssl md5 -c
done \
| awk '/^\(stdin\)/{ print $2 }'
